/**
 * Module dependencies.
 */
var express = require('express'),
    path = require('path');

/**
 * Controllers
 */
var homeController = require('./controllers/home');


/**
 * Express
 */
var app = express();
 
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(require('connect-assets')({
  src: 'public',
  helperContext: app.locals
}));
app.use(express.favicon());
app.use(express.logger('dev'));
/**
 * Will Be Removed in connect 3.0
 * use urlencoded() and json() as replacement
 
app.use(express.bodyParser());
 */
app.use(express.urlencoded());
app.use(express.json());
app.use(express.methodOverride());
app.use(express.cookieParser('YouShallNotPassWord'));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}



/**
 * Express Routes
 */
app.get("/", homeController);


/**
 * Start Server
 */
app.listen(app.get('port'), function() {
  console.log("Express server listening on port %d in %s mode", app.get('port'), app.settings.env);
});
